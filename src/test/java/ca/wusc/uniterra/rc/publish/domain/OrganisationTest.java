/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.domain;

import static ca.wusc.uniterra.rc.publish.domain.OrganisationTest.getFixture;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class OrganisationTest {
    private static ObjectMapper mapper = new ObjectMapper();

    public static Organisation getFixture() {
        return Organisation.builder()
                .idCode("CHMTL")
                .translations(getTranslations())
                .build();
        //return new Organisation(null, "ORG001", getTranslations());
    }

    @SneakyThrows
    public static List<Organisation.Translation> getTranslations() {
        List<Organisation.Translation> versions = new ArrayList<>();
        versions.add(Organisation.Translation.builder()
                .langIso("fr")
                .code("CH")
                .name("Les Glorieux")
                .build());
        versions.add(Organisation.Translation.builder()
                .langIso("en")
                .code("CH")
                .name("The habs")
                .build());
        return versions;
    }

    private Organisation instance;

    public OrganisationTest() {
    }

    @Before
    public void setUp() {
        instance = getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }
    
    @Test
    public void testEntityTitle() {
        System.out.println("Organisation test suite");
    }

    @Test
    public void testToString() {
        System.out.println("testToString");
        System.out.println(instance.toString());
    }
    @Test
    public void testWriteJsonObject() throws JsonProcessingException {
        System.out.println("testWriteJsonObject");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Pretty result = " + result);
        System.out.println("Result = " + mapper.writeValueAsString(instance));
    }
    
    
    @Test
    public void testReadJsonObject() throws JsonProcessingException, IOException {
        System.out.println("testReadJsonObject");
        String input = mapper.writeValueAsString(instance);
        Organisation result = mapper.readValue(input, Organisation.class);
        assertNotNull(result);
        assertEquals("CHMTL", result.getIdCode());
        assertEquals("The habs", result.getTranslation("en").getName());
        System.out.println("Result = " + result.toString());
    }

    @Test
    public void testCanEqualFalse() {
        System.out.println("canEqualFalse");
        Object other = "Hello";
        boolean expResult = false;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testCanEqualTrue() {
        System.out.println("canEqualTrue");
        Organisation other = getFixture();
        boolean expResult = true;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEqualsFalse() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCodeEquals");
        int result = instance.hashCode();
        Organisation other = getFixture();
        assertEquals(other.hashCode(), result);
        System.out.println("hashcode = " + result);
    }

    @Test
    public void testGetPkey() {
        System.out.println("getPkey");
        Long expResult = null;
        Long result = instance.getPkey();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetIdCode() {
        System.out.println("getIdCode");
        String expResult = "CHMTL";
        String result = instance.getIdCode();
        assertEquals(expResult, result);
    }

}
