/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class JobPostCriteriasTest {

    public JobPostCriteriasTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testJobPostCriteriasBuilderDefaults() {
        System.out.println("testJobPostCriteriasBuilderDefaults");
                JobPostCriterias filter = JobPostCriterias.builder()
                .country("MWI|GHA|BKI")
                .language("fr|en")
                .competence("AGRO|MRKT")
                .duration(24)
                .build();

        assertEquals("MWI|GHA|BKI", filter.getCountry());
        assertEquals("fr|en", filter.getLanguage());
        System.out.println("Criterias = " + filter.toString());
    }

    /*  
    @Test
    public void testBuilder() {
        System.out.println("builder");
        JobPostCriterias.JobPostCriteriasBuilder expResult = null;
        JobPostCriterias.JobPostCriteriasBuilder result = JobPostCriterias.builder();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetCountry() {
        System.out.println("getCountry");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getCountry();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetLanguage() {
        System.out.println("getLanguage");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getLanguage();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetCompetence() {
        System.out.println("getCompetence");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getCompetence();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetDuration() {
        System.out.println("getDuration");
        JobPostCriterias instance = null;
        int expResult = 0;
        int result = instance.getDuration();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetSector() {
        System.out.println("getSector");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getSector();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetOrganisation() {
        System.out.println("getOrganisation");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getOrganisation();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetProgram() {
        System.out.println("getProgram");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getProgram();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    @Test
    public void testGetStream() {
        System.out.println("getStream");
        JobPostCriterias instance = null;
        String expResult = "";
        String result = instance.getStream();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
     */
}
