/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.repo;

import ca.wusc.uniterra.rc.publish.domain.Organisation;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author jacques
 */
public interface OrganisationRepo extends CrudRepository<Organisation, Long> {

    public Organisation findByIdCode(String code);

}
