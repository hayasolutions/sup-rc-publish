/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Stream;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.SneakyThrows;

/**
 *
 * @author jacques
 */
@Converter
public class StreamTranslationConverter implements AttributeConverter<List<Stream.Translation>, String> {
    
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    @SneakyThrows
    public String convertToDatabaseColumn(List<Stream.Translation> versions) {
        System.out.println("StreamVersionConverter to db");
            return MAPPER.writeValueAsString(versions);
    }

    @Override
    @SneakyThrows
    public List<Stream.Translation> convertToEntityAttribute(String dbdata) {
        System.out.println("StreamVersionConverter from db");
            CollectionType typeReference;
            typeReference = (CollectionType) TypeFactory.defaultInstance()
                    .constructCollectionLikeType(List.class, Stream.Translation.class);
            List<Stream.Translation> result = MAPPER.readValue(dbdata, typeReference);
            return result;
    }
    
}
