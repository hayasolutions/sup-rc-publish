/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @author jacques
 */
@Builder
@AllArgsConstructor
@ToString
public class JobPostCriterias {
    public static ObjectMapper mapper = new ObjectMapper();

    public static class JobPostCriteriasBuilder {
        private String country = "*";
        private String language = "en";
        private String competence = "*";
        private int duration = 12;
        private String sector = "*";
        private String organisation = "*";
        private String program = "*";
        private String stream = "*";
    }

    @Getter
    private final String country;
    @Getter
    private final String language;
    @Getter
    private final String competence;
    @Getter
    private final int duration;
    @Getter
    private final String sector;
    @Getter
    private final String organisation;
    @Getter
    private final String program;
    @Getter
    private final String stream;
    
    /*Methods*/
    
    public String asJson() throws JsonProcessingException {
        return mapper.writeValueAsString(this);
    }
}
