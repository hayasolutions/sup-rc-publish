/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.response;

import ca.wusc.uniterra.rc.publish.dto.CompetenceDto;
import ca.wusc.uniterra.rc.publish.dto.CountryDto;
import ca.wusc.uniterra.rc.publish.dto.JobPostDto;
import ca.wusc.uniterra.rc.publish.dto.LanguageDto;
import ca.wusc.uniterra.rc.publish.dto.OrganisationDto;
import ca.wusc.uniterra.rc.publish.dto.ProgramDto;
import ca.wusc.uniterra.rc.publish.dto.SectorDto;
import ca.wusc.uniterra.rc.publish.dto.StreamDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

/**
 *
 * @author jacques
 */
@Builder
@AllArgsConstructor @ToString
public class JobPostContent {


    @Getter
    private final JobPostDto jobpost;
    @Getter
    private final CountryDto country;
    @Getter
    private final LanguageDto language;
    @Getter
    private final OrganisationDto organisation;
    @Getter
    private final ProgramDto program;
    @Getter
    private final SectorDto sector;
    @Getter
    private final StreamDto stream;
    @Getter
    private final CompetenceDto competence;

}
