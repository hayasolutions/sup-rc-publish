/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.domain;

import static ca.wusc.uniterra.rc.publish.domain.LanguageTest.getFixture;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class LanguageTest {
    private static ObjectMapper mapper = new ObjectMapper();
    
    public static Language getFixture() {
        return Language.builder()
                .idCode("ru")
                .translations(getTranslations())
                .build();
    }
    
    @SneakyThrows
    public static List<Language.Translation> getTranslations() {
        List<Language.Translation> versions = new ArrayList<>();
        versions.add(Language.Translation.builder()
                .langIso("fr")
                .name("Russe")
                .build());
        versions.add(Language.Translation.builder()
                .langIso("en")
                .name("Russian")
                .build());
        return versions;
    }

    private Language instance;

    public LanguageTest() {
    }

    @Before @SneakyThrows
    public void setUp() {
        instance = getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testToString() {
        System.out.println("testToString");
        System.out.println(instance.toString());;
    }
    
    @Test
    public void testWriteJsonObject() throws JsonProcessingException {
        System.out.println("testWriteJsonObject");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Pretty result = " + result);
        System.out.println("Result = " + mapper.writeValueAsString(instance));
    }
    
    
    @Test
    public void testReadJsonObject() throws JsonProcessingException, IOException {
        System.out.println("testReadJsonObject");
        String input = mapper.writeValueAsString(instance);
        Language result = mapper.readValue(input, Language.class);
        assertNotNull(result);
        assertEquals("ru", result.getIdCode());
        assertEquals("Russian", result.getTranslation("en").getName());
        System.out.println("Result = " + result.toString());
    }
    

    @Test
    public void testGetTranslation() {
        System.out.println("testToString");
        assertEquals("Russe", instance.getTranslation("fr").getName());
        assertEquals("Russian", instance.getTranslation("en").getName());
    }
   
    @Test
    public void testCanEqualFalse() {
        System.out.println("canEqualFalse");
        Object other = "Hello";
        boolean expResult = false;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testCanEqualTrue() {
        System.out.println("canEqualTrue");
        Language other = getFixture();
        boolean expResult = true;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEqualsFalse() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCodeEquals");
        int result = instance.hashCode();
        Language other = getFixture();
        assertEquals(other.hashCode(), result);
        System.out.println("hashcode = " + result);
    }

    @Test
    public void testGetPkey() {
        System.out.println("getPkey");
        Long expResult = null;
        Long result = instance.getPkey();
        assertEquals(expResult, result);
    }

}
