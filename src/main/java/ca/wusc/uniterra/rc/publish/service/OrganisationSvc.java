/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;
import ca.wusc.uniterra.rc.publish.domain.Organisation;
import ca.wusc.uniterra.rc.publish.repo.OrganisationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author jacques
 */
@Service
public class OrganisationSvc {

    @Autowired
    private OrganisationRepo countryRepo;
    
    @Cacheable("organisation")
    public Organisation getOrganisation(String idCode) {
        return countryRepo.findByIdCode(idCode);
    }
    
}
