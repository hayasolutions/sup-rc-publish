/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Competence;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class CompetenceDto extends BaseDto {

    public static CompetenceDto of(Competence instance, String isoLang) {
        return new CompetenceDto(instance.getIdCode(), instance.getTranslation(isoLang));
    }

    @Getter
    private String formalCode;
    @Getter
    private String translatedCode;
    @Getter
    private String name;
    

    public CompetenceDto(String idCode, Competence.Translation version) {
        this.formalCode = idCode;
        this.translatedCode = version.getCode();
        this.name = version.getName();
    }

}
