/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Competence;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.SneakyThrows;

/**
 *
 * @author jacques
 */
@Converter
public class CompetenceTranslationConverter implements AttributeConverter<List<Competence.Translation>, String> {
    
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    @SneakyThrows
    public String convertToDatabaseColumn(List<Competence.Translation> versions) {
            return MAPPER.writeValueAsString(versions);
    }

    @Override
    @SneakyThrows
    public List<Competence.Translation> convertToEntityAttribute(String dbdata) {
            CollectionType typeReference;
            typeReference = (CollectionType) TypeFactory.defaultInstance()
                    .constructCollectionLikeType(List.class, Competence.Translation.class);
            List<Competence.Translation> result = MAPPER.readValue(dbdata, typeReference);
            return result;
    }
    
}
