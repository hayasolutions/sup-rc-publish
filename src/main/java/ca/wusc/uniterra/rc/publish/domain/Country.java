package ca.wusc.uniterra.rc.publish.domain;

import ca.wusc.uniterra.rc.publish.utils.CountryTranslationConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ISO Alpha-2 country codes
 * (http://www.nationsonline.org/oneworld/country_code_list.htm)
 *
 * @author jacques
 */
@Entity
@Table(name = "rf_country")
@EqualsAndHashCode(of = "idCode")
@NoArgsConstructor
@ToString
public class Country implements Serializable {

    private static final long serialVersionUID = 1L;

    @Builder
    @ToString
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"langIso","name"})
    public static class Translation {

        @Getter
        private final String langIso;
        @Getter
        private final String name;
    }

    //Persistent fields
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long pkey;

    @Getter
    @Column(name = "id_code", unique = true, nullable = false, length = 3)
    private String idCode;
    
    @Getter @JsonIgnore
    @Version @Column(name = "record_version")
    private Integer version;

    @Getter
    @Column(nullable = false, length = 2)
    private String language;

    @JsonProperty
    @Column(name = "json_translations")
    @Getter(AccessLevel.PACKAGE)
    @Convert(converter = CountryTranslationConverter.class)
    private List<Country.Translation> translations;

    
    
    
    public Country.Translation getTranslation(String iso) {
        return (Country.Translation) translations.stream()
                .filter(v -> v.getLangIso().equals(iso)).findFirst()
                .orElse(translations.get(0));
    }

    @Builder
    public Country(String idCode, String language, List<Translation> translations) {
        this.idCode = idCode;
        this.language = language;
        this.translations = translations;
    }
}
