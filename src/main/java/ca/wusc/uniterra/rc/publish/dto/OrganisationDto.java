/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Organisation;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class OrganisationDto extends BaseDto {

    public static OrganisationDto of(Organisation instance, String translation) {
        return new OrganisationDto(instance.getIdCode(), instance.getTranslation(translation));
    }

    @Getter
    private String idCode;
    @Getter
    private String translatedCode;
    @Getter
    private String name;

    public OrganisationDto(String idCode, Organisation.Translation version) {
        this.idCode = idCode;
        this.translatedCode = version.getCode();
        this.name = version.getName();
    }
    
}
