/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.JobPost;
import ca.wusc.uniterra.rc.publish.domain.JobPostTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jacques
 */
public class JobPostDtoTest {
    
    public static JobPostDto getFixture(String isoLang) {
        JobPost post = JobPostTest.getFixture();
        CountryDto country = CountryDtoTest.getFixture(isoLang);
        return JobPostDto.of(post, isoLang);
    }
    
    private ObjectMapper mapper = new ObjectMapper();
    private JobPostDto instance;
    
    
    public JobPostDtoTest() {
    }
    
    @Before
    public void setUp() {
        instance = getFixture("en");
    }
    
    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testSerializing() throws JsonProcessingException {
        System.out.println("testSerializing");
        String result = instance.toJson();
        System.out.println("Serialized JobPostDto = " + result);
    }
    @Test
    public void testPrettySerializing() throws JsonProcessingException {
        System.out.println("testPrettySerializing");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Serialized JobPostDto = " + result);
    }
    
}
