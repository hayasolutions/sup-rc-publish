package ca.wusc.uniterra.rc.publish.domain;

import ca.wusc.uniterra.rc.publish.utils.CompetenceTranslationConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "rf_competence")
@EqualsAndHashCode(of = "idCode")
@NoArgsConstructor
@ToString
public class Competence implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Builder
    @ToString
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"langIso", "code"})
    public static class Translation  {

        @Getter
        private final String langIso;
        @Getter
        private final String code;
        @Getter
        private final String name;
    }

    //Persistent fields
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long pkey;
    
    @Getter
    @Column(unique = true, nullable = false, length = 10)
    private String idCode;
    
    @Getter
    @Version @Column(name = "record_version")
    @JsonIgnore
    private Integer version;
  
    @JsonProperty
    @Column(name = "json_translations")
    @Convert(converter = CompetenceTranslationConverter.class)
    @Getter(AccessLevel.PACKAGE)
    private List<Competence.Translation> translations;
    
    /*Methods*/
    public Competence.Translation getTranslation(String iso) {
        return (Competence.Translation) translations.stream()
                .filter(v -> v.getLangIso().equals(iso)).findFirst()
                .orElse(translations.get(0));
    }

    @Builder
    public Competence(String idCode, List<Translation> translations) {
        this.idCode = idCode;
        this.translations = translations;
    }
    
    
    
}
