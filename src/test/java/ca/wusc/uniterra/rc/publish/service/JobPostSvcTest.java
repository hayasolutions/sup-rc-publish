/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import ca.wusc.uniterra.rc.publish.domain.Competence;
import ca.wusc.uniterra.rc.publish.domain.CompetenceTest;
import ca.wusc.uniterra.rc.publish.domain.Country;
import ca.wusc.uniterra.rc.publish.domain.CountryTest;
import ca.wusc.uniterra.rc.publish.domain.JobPost;
import ca.wusc.uniterra.rc.publish.domain.JobPostTest;
import ca.wusc.uniterra.rc.publish.domain.Language;
import ca.wusc.uniterra.rc.publish.domain.LanguageTest;
import ca.wusc.uniterra.rc.publish.domain.Organisation;
import ca.wusc.uniterra.rc.publish.domain.OrganisationTest;
import ca.wusc.uniterra.rc.publish.domain.Program;
import ca.wusc.uniterra.rc.publish.domain.ProgramTest;
import ca.wusc.uniterra.rc.publish.domain.Sector;
import ca.wusc.uniterra.rc.publish.domain.SectorTest;
import ca.wusc.uniterra.rc.publish.domain.Stream;
import ca.wusc.uniterra.rc.publish.domain.StreamTest;
import ca.wusc.uniterra.rc.publish.dto.CountryDto;
import ca.wusc.uniterra.rc.publish.repo.JobPostRepo;
import ca.wusc.uniterra.rc.publish.response.JobPostContent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author jacques
 */


@RunWith(PowerMockRunner.class)
@PrepareForTest({CountryDto.class})
public class JobPostSvcTest {
    private ObjectMapper mapper = new ObjectMapper();
    @Mock
    CountrySvc countrySvc;
    @Mock
    LanguageSvc languageSvc;
    @Mock
    OrganisationSvc organisationSvc;
    @Mock
    ProgramSvc programSvc;
    @Mock
    SectorSvc sectorSvc;
    @Mock
    StreamSvc streamSvc;
    @Mock
    CompetenceSvc competenceSvc;
    @Mock
    JobPostRepo jobRepo;
    
    @InjectMocks
    private JobPostSvc instance;

    public JobPostSvcTest() {
    }

    @Before
    public void setUp() {
        //this.instance = new JobPostSvc();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() {
        this.instance = null;
    }
    
    

    @Test
    public void createFilterWithBuilder() {
        System.out.println("testCreateFileterWithBuilder");
    }

    @Test
    public void testGetJobPostDto() throws JsonProcessingException {
        System.out.println("testGetJobPostDto");

        JobPost post = JobPostTest.getFixture();
        Country russia = CountryTest.getFixture();
        Language russian = LanguageTest.getFixture();
        Organisation chmtl = OrganisationTest.getFixture();
        Program captain = ProgramTest.getFixture();
        Sector juice = SectorTest.getFixture();
        Stream leave = StreamTest.getFixture();
        Competence music = CompetenceTest.getFixture();

        when(jobRepo.findByIdCode("ABCDEF")).thenReturn(post);
        when(countrySvc.getCountry("RU")).thenReturn(russia);
        when(languageSvc.getLanguage("ru")).thenReturn(russian);
        when(organisationSvc.getOrganisation("CHMTL")).thenReturn(chmtl);
        when(programSvc.getProgram("CB")).thenReturn(captain);
        when(sectorSvc.getSector("JUICE")).thenReturn(juice);
        when(streamSvc.getStream("LTCB")).thenReturn(leave);
        when(competenceSvc.getCompetence("MUSIC")).thenReturn(music);

        JobPostContent result = instance.getJobPostContent("ABCDEF", "fr");
        System.out.println("Result JobPOstContent " +
                mapper.writer().withDefaultPrettyPrinter().writeValueAsString(result));
        
    }

    /**
     * Test of getJobs method, of class JobSvc.
     */
    /*
    @Test
    public void testGetJobs() {
        System.out.println("getJobs");
        JobPostSvc.Criterias criterias = null;
        JobPostSvc instance = new JobPostSvc();
        List<Job> expResult = null;
        List<Job> result = instance.getJobs(criterias);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
     */
}
