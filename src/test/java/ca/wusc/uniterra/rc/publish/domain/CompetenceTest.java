/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class CompetenceTest {
    private static ObjectMapper mapper = new ObjectMapper();
    
    public static Competence getFixture() {
        return Competence.builder()
                .idCode("MUSIC")
                .translations(getTranslations())
                .build();
    }
    
    @SneakyThrows
    public static List<Competence.Translation> getTranslations() {
        List<Competence.Translation> translations = new ArrayList<>();
        translations.add(Competence.Translation.builder()
                .langIso("fr")
                .code("MUS")
                .name("Musique")
                .build());
        translations.add(Competence.Translation.builder()
                .langIso("en")
                .code("MUSIC")
                .name("Music")
                .build());
        return translations;
    }
    
    private Competence instance;

    public CompetenceTest() {
    }

    @Before
    public void setUp() {
        instance = CompetenceTest.getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testToString() {
        System.out.println("testToString");
        System.out.println(instance.toString());
    }
    
    
    @Test
    public void testWriteJsonObject() throws JsonProcessingException {
        System.out.println("testWriteJsonObject");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Pretty result = " + result);
        System.out.println("Result = " + mapper.writeValueAsString(instance));
    }
    
    
    @Test
    public void testReadJsonObject() throws JsonProcessingException, IOException {
        System.out.println("testReadJsonObject");
        String input = mapper.writeValueAsString(instance);
        Competence result = mapper.readValue(input, Competence.class);
        assertNotNull(result);
        assertEquals("MUSIC", result.getIdCode());
        assertEquals("Music", result.getTranslation("en").getName());
        System.out.println("Result = " + result.toString());
    }
    

    @Test
    public void testCanEqualFalse() {
        System.out.println("canEqualFalse");
        Object other = "Hello";
        boolean expResult = false;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testCanEqualTrue() {
        System.out.println("canEqualTrue");
        Competence other = getFixture();
        boolean expResult = true;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testEqualsFalse() {
        System.out.println("testEqualsFalse");
        Object o = null;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("testHashCodeEquals");
        int result = instance.hashCode();
        Competence other = getFixture();
        assertEquals(other.hashCode(), result);
        System.out.println("hashcode = " + result);
    }

    @Test
    public void testGetPkey() {
        System.out.println("testGetPkey");
        Long expResult = null;
        Long result = instance.getPkey();
        assertEquals(expResult, result);
    }
}
