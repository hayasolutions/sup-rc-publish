/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Language;
import ca.wusc.uniterra.rc.publish.domain.LanguageTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class LanguageTranslationConverterTest {

    private static ObjectMapper mapper = new ObjectMapper();

    private LanguageTranslationConverter instance;

    public LanguageTranslationConverterTest() {
    }

    @Before
    public void setUp() {
        instance = new LanguageTranslationConverter();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of convertToDatabaseColumn method, of class
     * LanguageTranslationConverterTest.
     */
    @Test
    public void testConvertToDatabaseColumn() throws JsonProcessingException {
        System.out.println("testConvertToDatabaseColumn");
        List<Language.Translation> trns = LanguageTest.getTranslations();

        String expResult = mapper.writeValueAsString(trns);
        String result = instance.convertToDatabaseColumn(trns);
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testConvertToEntityAttribute() throws JsonProcessingException {
        System.out.println("testConvertToEntityAttribute");
        List<Language.Translation> expResult = LanguageTest.getTranslations();
        String input = mapper.writeValueAsString(expResult);
        List<Language.Translation> result = instance.convertToEntityAttribute(input);
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        System.out.println(result);

    }
}
