/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Program;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class ProgramDto  extends BaseDto {

    public static ProgramDto of(Program instance, String translation) {
        return new ProgramDto(instance.getIdCode(), instance.getTranslation(translation));
    }

    @Getter
    private String idCode;
    @Getter
    private String translatedCode;
    @Getter
    private String name;

    public ProgramDto(String idCode, Program.Translation version) {
        this.idCode = idCode;
        this.translatedCode = version.getCode();
        this.name = version.getName();
    }
}
