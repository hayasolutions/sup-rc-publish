/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jacques
 * Created: 17-Feb-2017
 * Version: 0.0.1
 * Type: MySQL
 */

/* Structure */


/*Data*/

INSERT INTO `db_wusc`.`rf_organisation`(`id_code`, `json_translations`) VALUES ('CECI',
'[{"langIso":"fr","code":"CECI","name":"Centre d''étude et de coopération internationale"},
  {"langIso":"en","code":"CECI","name":"Centre for International Studies and Cooperation"}]');

INSERT INTO `db_wusc`.`rf_organisation`(`id_code`, `json_translations`) VALUES ('WUSC',
'[{"langIso":"fr","code":"EUMC","name":"Entraide universitaire mondiale Canada"},
  {"langIso":"en","code":"WUSC","name":"World University Service Canada"}]');

INSERT INTO `db_wusc`.`rf_program`(`id_code`, `json_translations`) VALUES ('UNITERRA',
'[{"langIso":"fr","code":"UNITERRA","name":"Uniterra"},
  {"langIso":"en","code":"UNITERRA","name":"Uniterra"}]');

INSERT INTO `db_wusc`.`rf_program`(`id_code`, `json_translations`) VALUES ('PROPEL',
'[{"langIso":"fr","code":"PROPEL","name":"Promotion de production agricole régionale par la création d''entreprises et de réseaux"},
  {"langIso":"en","code":"PROPEL","name":"Promotion of Regional Opportunies for Produce through Enterprises and Linkages"}]');

INSERT INTO `db_wusc`.`rf_competence`(`id_code`, `json_translations`) VALUES ('AGRN',
'[{"langIso":"fr","code":"AGRN","name":"Agronomie"},
  {"langIso":"en","code":"AGRN","name":"Agronimy"}]');

INSERT INTO `db_wusc`.`rf_competence`(`id_code`, `json_translations`) VALUES ('MRKT',
'[{"langIso":"fr","code":"MRKT","name":"Marketing"},
  {"langIso":"en","code":"MRKT","name":"Marketing"}]');

INSERT INTO `db_wusc`.`rf_competence`(`id_code`, `json_translations`) VALUES ('IT',
'[{"langIso":"fr","code":"TI","name":"Technologies de l''Information"},
  {"langIso":"en","code":"IT","name":"Information technologies"}]');

INSERT INTO `db_wusc`.`rf_competence`(`id_code`, `json_translations`) VALUES ('LRN',
'[{"langIso":"fr","code":"FOR","name":"Formation"},
  {"langIso":"en","code":"LRN","name":"Training"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('BOL', 'es',
'[{"langIso":"fr","name":"Bolivie"},
  {"langIso":"en","name":"Bolivia"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('CAD', 'en',
'[{"langIso":"fr","name":"Canada"},
  {"langIso":"en","name":"Canada"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('BFA', 'fr',
'[{"langIso":"fr","name":"Burkina Faso"},
  {"langIso":"en","name":"Burkina Faso"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('GHA', 'en',
'[{"langIso":"fr","name":"Ghana"},
  {"langIso":"en","name":"Ghana"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('GTM', 'es',
'[{"langIso":"fr","name":"Guatemala"},
  {"langIso":"en","name":"Guatemala"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('GIN', 'fr',
'[{"langIso":"fr","name":"Guinée"},
  {"langIso":"en","name":"Guinea"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('HTI', 'fr',
'[{"langIso":"fr","name":"Haïti"},
  {"langIso":"en","name":"Haiti"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('MWI', 'en',
'[{"langIso":"fr","name":"Malawi"},
  {"langIso":"en","name":"Malawi"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('MLI', 'fr',
'[{"langIso":"fr","name":"Mali"},
  {"langIso":"en","name":"Mali"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('MNG', 'en',
'[{"langIso":"fr","name":"Mongolie"},
  {"langIso":"en","name":"Mongolia"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('PER', 'es',
'[{"langIso":"fr","name":"Peru"},
  {"langIso":"en","name":"Pérou"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('NPL', 'en',
'[{"langIso":"fr","name":"Népal"},
  {"langIso":"en","name":"Nepal"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('PHL', 'en',
'[{"langIso":"fr","name":"Philippines"},
  {"langIso":"en","name":"Philippines"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('SEN', 'fr',
'[{"langIso":"fr","name":"Sénégal"},
  {"langIso":"en","name":"Senegal"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('TZA', 'en',
'[{"langIso":"fr","name":"Tanzannie"},
  {"langIso":"en","name":"Tanzania"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('LKA', 'en',
'[{"langIso":"fr","name":"Sri Lanka"},
  {"langIso":"en","name":"Sri Lanka"}]');

INSERT INTO `db_wusc`.`rf_country`(`id_code`, `language`, `json_translations`) VALUES ('VNM', 'fr',
'[{"langIso":"fr","name":"Vietnam"},
  {"langIso":"en","name":"Viet Nam"}]');

INSERT INTO `db_wusc`.`rf_language`(`id_code`, `json_translations`) VALUES ('fr',
'[{"langIso":"fr","name":"Français"},
  {"langIso":"en","name":"French"}]');

INSERT INTO `db_wusc`.`rf_language`(`id_code`, `json_translations`) VALUES ('en',
'[{"langIso":"fr","name":"Anglais"},
  {"langIso":"en","name":"English"}]');

INSERT INTO `db_wusc`.`rf_language`(`id_code`, `json_translations`) VALUES ('es',
'[{"langIso":"fr","name":"Espagnol"},
  {"langIso":"en","name":"Spanish"}]');

