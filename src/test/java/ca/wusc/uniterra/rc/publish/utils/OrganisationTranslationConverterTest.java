/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Organisation;
import ca.wusc.uniterra.rc.publish.domain.OrganisationTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class OrganisationTranslationConverterTest {

    private static ObjectMapper mapper = new ObjectMapper();

    private OrganisationTranslationConverter instance;

    public OrganisationTranslationConverterTest() {
    }

    @Before
    public void setUp() {
        instance = new OrganisationTranslationConverter();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of convertToDatabaseColumn method, of class
 OrganisationTranslationConverterTest.
     */
    @Test
    public void testConvertToDatabaseColumn() {
        System.out.println("testConvertToDatabaseColumn");
        List<Organisation.Translation> versions = OrganisationTest.getTranslations();

        String expResult = "[{\"langIso\":\"fr\",\"code\":\"CH\",\"name\":\"Les Glorieux\"},"
                + "{\"langIso\":\"en\",\"code\":\"CH\",\"name\":\"The habs\"}]";
        String result = instance.convertToDatabaseColumn(versions);
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testConvertToEntityAttribute() {
        System.out.println("testConvertToEntityAttribute");
        String input = "[{\"langIso\":\"fr\",\"code\":\"CH\",\"name\":\"Les Glorieux\"},"
                + "{\"langIso\":\"en\",\"code\":\"CH\",\"name\":\"The habs\"}]";
        List<Organisation.Translation> expResult = OrganisationTest.getTranslations();
        List<Organisation.Translation> result = instance.convertToEntityAttribute(input);
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        System.out.println(result);

    }
}
