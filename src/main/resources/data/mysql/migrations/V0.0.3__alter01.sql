/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jacques
 * Created: 17-Feb-2017
 * Version: 0.0.1
 * Type: MySQL
 */

/* Structure */

ALTER TABLE `db_wusc`.`rf_country` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_language` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_organisation` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_program` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_sector` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_stream` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rf_competence` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;

ALTER TABLE `db_wusc`.`rc_jobpost` 
ADD COLUMN `record_version` INT NULL DEFAULT 0 AFTER `id_code`;



/*Data*/

