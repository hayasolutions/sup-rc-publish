/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import ca.wusc.uniterra.rc.publish.domain.JobPost;
import ca.wusc.uniterra.rc.publish.dto.CompetenceDto;
import ca.wusc.uniterra.rc.publish.dto.CountryDto;
import ca.wusc.uniterra.rc.publish.dto.JobPostDto;
import ca.wusc.uniterra.rc.publish.dto.LanguageDto;
import ca.wusc.uniterra.rc.publish.dto.OrganisationDto;
import ca.wusc.uniterra.rc.publish.dto.ProgramDto;
import ca.wusc.uniterra.rc.publish.dto.SectorDto;
import ca.wusc.uniterra.rc.publish.dto.StreamDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ca.wusc.uniterra.rc.publish.repo.JobPostRepo;
import ca.wusc.uniterra.rc.publish.response.JobPostContent;
import javax.transaction.Transactional;

/**
 *
 * @author jacques
 */
@Service
public class JobPostSvc {

    @Autowired
    private JobPostRepo jobRepo;
    @Autowired
    private CountrySvc countrySvc;
    @Autowired
    private LanguageSvc languageSvc;
    @Autowired
    private OrganisationSvc organisationSvc;
    @Autowired
    private ProgramSvc programSvc;
    @Autowired
    private SectorSvc sectorSvc;
    @Autowired
    private StreamSvc streamSvc;
    @Autowired
    private CompetenceSvc competenceSvc;

    @Transactional
    public JobPostContent getJobPostContent(String idCode, String translation) {
        JobPost post = jobRepo.findByIdCode(idCode);
        
        JobPostDto jobPostDto = JobPostDto.of(post, translation);
        
        CountryDto countryDto = 
               CountryDto.of(countrySvc.getCountry(post.getCountry()), translation);
        LanguageDto languageDto = 
                LanguageDto.of(languageSvc.getLanguage(post.getLanguage()), translation);
        OrganisationDto organisationDto = 
                OrganisationDto.of(organisationSvc.getOrganisation(post.getOrganisation()), translation);
        ProgramDto programDto = 
                ProgramDto.of(programSvc.getProgram(post.getProgram()), translation);
        SectorDto sectorDto = 
                SectorDto.of(sectorSvc.getSector(post.getSector()), translation);
        StreamDto streamDto = 
                StreamDto.of(streamSvc.getStream(post.getStream()), translation);
        CompetenceDto competenceDto = 
                CompetenceDto.of(competenceSvc.getCompetence(post.getCompetence()), translation);

        JobPostContent result = JobPostContent.builder()
                .jobpost(jobPostDto)
                .country(countryDto)
                .language(languageDto)
                .organisation(organisationDto)
                .program(programDto)
                .sector(sectorDto)
                .stream(streamDto)
                .competence(competenceDto)
                .build();
        
        return result;
    }

    public List<JobPost> getJobs(JobPostCriterias criterias) {
        return null;
    }

}
