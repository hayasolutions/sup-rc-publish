/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.JobPost;
import ca.wusc.uniterra.rc.publish.domain.JobPostTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class JobPostTranslationConverterTest {

    private static ObjectMapper mapper = new ObjectMapper();

    private JobPostTranslationConverter instance;

    public JobPostTranslationConverterTest() {
    }

    @Before
    public void setUp() {
        instance = new JobPostTranslationConverter();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of convertToDatabaseColumn method, of class
 JobPostTranslationConverterTest.
     */
    @Test
    public void testConvertToDatabaseColumn() {
        System.out.println("testConvertToDatabaseColumn");
        List<JobPost.Translation> versions = JobPostTest.getTranslations();

        String expResult = "[{\"langIso\":\"en\",\"title\":\"Job description title\",\"docLink\":\"Document link\","
                + "\"pdfLink\":\"Pdf link\",\"location\":\"Downtown\",\"excerpt\":\"This is an excerpt\"},"
                + "{\"langIso\":\"fr\",\"title\":\"Tître du poste\",\"docLink\":\"Lien Document\",\"pdfLink\":\"Lien Pdf\","
                + "\"location\":\"Centre-ville\",\"excerpt\":\"Voici un aperçu\"}]";
        byte[] result = instance.convertToDatabaseColumn(versions);
        String s = new String(result);
        assertEquals(expResult, s);
        System.out.println(s);
    }

    @Test
    public void testConvertToEntityAttribute() {
        System.out.println("testConvertToEntityAttribute");
        String input = "[{\"langIso\":\"en\",\"title\":\"Job description title\",\"docLink\":\"Document link\","
                + "\"pdfLink\":\"Pdf link\",\"location\":\"Downtown\",\"excerpt\":\"This is an excerpt\"},"
                + "{\"langIso\":\"fr\",\"title\":\"Tître du poste\",\"docLink\":\"Lien Document\",\"pdfLink\":\"Lien Pdf\","
                + "\"location\":\"Centre-ville\",\"excerpt\":\"Voici un aperçu\"}]";
        List<JobPost.Translation> expResult = JobPostTest.getTranslations();
        List<JobPost.Translation> result = instance.convertToEntityAttribute(input.getBytes());
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        System.out.println(result);

    }
}
