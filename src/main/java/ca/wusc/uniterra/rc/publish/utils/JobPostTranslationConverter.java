/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.JobPost;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import lombok.SneakyThrows;

/**
 *
 * @author jacques
 */
@Converter
public class JobPostTranslationConverter implements AttributeConverter<List<JobPost.Translation>, byte[]> {
    
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Override
    @SneakyThrows
    public byte[] convertToDatabaseColumn(List<JobPost.Translation> versions) {
        System.out.println("JobVersionConverter to db");
            return MAPPER.writeValueAsString(versions).getBytes();
    }

    @Override
    @SneakyThrows
    public List<JobPost.Translation> convertToEntityAttribute(byte[] dbdata) {
        System.out.println("JobVersionConverter from db");
            CollectionType typeReference;
            typeReference = (CollectionType) TypeFactory.defaultInstance()
                    .constructCollectionLikeType(List.class, JobPost.Translation.class);
            List<JobPost.Translation> result = MAPPER.readValue(dbdata, typeReference);
            return result;
    }
    
}
