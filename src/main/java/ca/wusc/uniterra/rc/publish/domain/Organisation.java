package ca.wusc.uniterra.rc.publish.domain;

import ca.wusc.uniterra.rc.publish.utils.OrganisationTranslationConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity @Table(name="rf_organisation")
@EqualsAndHashCode(of = "idCode")
@NoArgsConstructor @ToString
public class Organisation  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Builder
    @ToString
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"langIso", "code"})
    public static class Translation  {

        @Getter
        private final String langIso;
        @Getter
        private final String code;
        @Getter
        private final String name;
    }

    //Persistent fields
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long pkey;

    @Getter
    @Column(unique = true, nullable = false, length = 10)
    private String idCode; 
    
    @Getter
    @Version @Column(name =  "record_version")
    private Integer version;

    @JsonProperty
    @Getter(AccessLevel.PACKAGE)
    @Column(name = "json_translations")
    @Convert(converter = OrganisationTranslationConverter.class)
    private List<Organisation.Translation> translations;
    
    /*Methods*/
    
    
    public Organisation.Translation getTranslation(String iso) {
        return (Organisation.Translation) translations.stream()
                .filter(v -> v.getLangIso().equals(iso)).findFirst()
                .orElse(translations.get(0));
    }
    
    @Builder

    public Organisation(String idCode, List<Translation> translations) {
        this.idCode = idCode;
        this.translations = translations;
    }
    
}
