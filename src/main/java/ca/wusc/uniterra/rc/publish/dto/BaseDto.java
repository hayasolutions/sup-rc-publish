/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author jacques
 */
public class BaseDto {
    protected static ObjectMapper mapper = new ObjectMapper();
    
    public String toJson() throws JsonProcessingException {
        return mapper.writeValueAsString(this);
    }
    
}
