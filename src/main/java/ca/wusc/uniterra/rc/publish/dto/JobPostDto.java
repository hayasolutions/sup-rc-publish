/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.JobPost;
import ca.wusc.uniterra.rc.publish.utils.LocalDateSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDate;
import javax.persistence.Column;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class JobPostDto extends BaseDto {

    public static JobPostDto of(JobPost instance, String translation) {
        return new JobPostDto(instance, translation);
    }

    @Getter
    @Column(unique = true, nullable = false, length = 10)
    private String idCode;  //unique job opening ID
    @Getter
    @JsonSerialize(using = LocalDateSerializer.class)  
    private LocalDate publishDate;
    @Getter
    @JsonSerialize(using = LocalDateSerializer.class)  
    private LocalDate applyUntilDate;
    @Getter
    @JsonSerialize(using = LocalDateSerializer.class)  
    private LocalDate departureDate;
    @Getter
    private int duration;
    @Getter
    private String applyEmail;
    @Getter
    private String language;
    @Getter
    private final String title;
    @Getter
    private final String docLink;
    @Getter
    private final String pdfLink;
    @Getter
    private final String location;
    @Getter
    private final String excerpt;

    private JobPostDto(JobPost post, String translation) {
        this.idCode = post.getIdCode();
        this.publishDate = post.getPublishDate();
        this.applyUntilDate = post.getApplyUntilDate();
        this.departureDate = post.getDepartureDate();
        this.duration = post.getDuration();
        this.applyEmail = post.getApplyEmail();
        this.language = post.getLanguage();
        JobPost.Translation version = post.getTranslation(translation);
        this.title = version.getTitle();
        this.pdfLink = version.getPdfLink();
        this.docLink = version.getDocLink();
        this.location = version.getLocation();
        this.excerpt = version.getExcerpt();
    }
}
