/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Sector;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class SectorDto  extends BaseDto {

    public static SectorDto of(Sector instance, String translation) {
        return new SectorDto(instance.getIdCode(), instance.getTranslation(translation));
    }

    @Getter
    private String idCode;
    @Getter
    private String translatedCode;
    @Getter
    private String name;

    public SectorDto(String idCode, Sector.Translation version) {
        this.idCode = idCode;
        this.translatedCode = version.getCode();
        this.name = version.getName();
    }
    
    
}
