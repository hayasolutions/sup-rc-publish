/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import ca.wusc.uniterra.rc.publish.domain.Program;
import ca.wusc.uniterra.rc.publish.repo.ProgramRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author jacques
 */
@Service
public class ProgramSvc {

    @Autowired
    private ProgramRepo countryRepo;
    
    @Cacheable("program")
    public Program getProgram(String idCode) {
        return countryRepo.findByIdCode(idCode);
    }
    
}
