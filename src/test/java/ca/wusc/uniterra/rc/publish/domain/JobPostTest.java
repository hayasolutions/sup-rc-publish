/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.domain;

import static ca.wusc.uniterra.rc.publish.domain.JobPostTest.getFixture;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.After;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jacques
 */
public class JobPostTest {
    
    private static ObjectMapper mapper = new ObjectMapper();

    public static JobPost getFixture() {
        return JobPost.builder()
                .idCode("ABCDEF")
                .publishDate(LocalDate.of(2017, Month.MARCH, 1))
                .applyUntilDate(LocalDate.of(2017, Month.MARCH, 1))
                .departureDate(LocalDate.of(2017, Month.MARCH, 1))
                .duration(12)
                .applyEmail("hello@world.org")
                .organisation("CHMTL")
                .country("RU")
                .program("CB")
                .language("ru")
                .sector("JUICE")
                .competence("MUSIC")
                .stream("LTCB")
                .translations(getTranslations())
                .build();
    }

    @SneakyThrows
    public static List<JobPost.Translation> getTranslations() {
        List<JobPost.Translation> translations = new ArrayList<>();
        translations.add(JobPost.Translation.builder()
                .docLink("Document link")
                .pdfLink("Pdf link")
                .excerpt("This is an excerpt")
                .langIso("en")
                .location("Downtown")
                .title("Job description title")
                .build()
        );
        translations.add(JobPost.Translation.builder()
                .docLink("Lien Document")
                .pdfLink("Lien Pdf")
                .excerpt("Voici un aperçu")
                .langIso("fr")
                .location("Centre-ville")
                .title("Tître du poste")
                .build()
        );
        return translations;
    }

    private JobPost instance;

    public JobPostTest() {
    }

    @Before
    public void setUp() {
        instance = getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testToString() {
        System.out.println("testToString");
        System.out.println(instance.toString());
    }
    
    @Test
    public void testToInlineJson() throws JsonProcessingException {
        System.out.println("testToInlineJson");
        String inline = mapper.writeValueAsString(instance);
        System.out.println(inline);
    }
    

    @Test
    public void testToPrettyPrint() throws JsonProcessingException {
        System.out.println("testToPrettyPrint");
        String pretty = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println(pretty);
    }

    @Test
    public void testFromJson() throws IOException  {
        System.out.println("testFromJson");
        System.out.println("****************  Resolve deserialization ***********");
        String input = mapper.writeValueAsString(instance);
        JobPost result = mapper.readValue(input, JobPost.class);
        assertEquals(result, instance);
        System.out.println("Result = " + result.toString());
    }
    
    @Test
    public void testGetTranslation() {
        System.out.println("testToString");
        assertEquals("Centre-ville", instance.getTranslation("fr").getLocation());
        assertEquals("Downtown", instance.getTranslation("en").getLocation());
    }

    @Test
    public void testCanEqualFalse() {
        System.out.println("canEqualFalse");
        Object other = "Hello";
        boolean expResult = false;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testCanEqualTrue() {
        System.out.println("canEqualTrue");
        JobPost other = getFixture();
        boolean expResult = true;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEqualsFalse() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCodeEquals");
        System.out.println("hashcode = " + instance.hashCode());
    }

    @Test
    public void testGetPkey() {
        System.out.println("getPkey");
        Long expResult = null;
        Long result = instance.getPkey();
        assertEquals(expResult, result);
    }

}
