package ca.wusc.uniterra.rc.publish.domain;

import ca.wusc.uniterra.rc.publish.utils.LanguageTranslationConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * ISO 639-1 : 2 letters language codes
 * (https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
 *
 * @author jacques
 */
@Entity
@Table(name = "rf_language")
@EqualsAndHashCode(of = "idCode")
@NoArgsConstructor
@ToString
public class Language implements Serializable {

    private static final long serialVersionUID = 1L;

    @Builder
    @ToString
    @AllArgsConstructor
    @EqualsAndHashCode(of = {"langIso","name"})
    public static class Translation {

        @Getter
        private final String langIso;
        @Getter
        private final String name;
    }

    //Persistent fields
    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long pkey;

    @Getter
    @Column(unique = true, nullable = false, length = 2)
    private String idCode;
    
    @Getter
    @Version @Column(name = "record_version")
    private Integer version;

    @JsonProperty
    @Column(name = "json_translations")
    @Getter(AccessLevel.PACKAGE)
    @Convert(converter = LanguageTranslationConverter.class)
    public List<Language.Translation> translations;

    /*Methods*/
    public Language.Translation getTranslation(String iso) {
        return (Language.Translation) translations.stream()
                .filter(v -> v.getLangIso().equals(iso)).findFirst()
                .orElse(translations.get(0));
    }
    
    @Builder
    public Language(String idCode, List<Translation> translations) {
        this.idCode = idCode;
        this.translations = translations;
    }
    

}
