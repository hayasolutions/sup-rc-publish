/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Country;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class CountryDto extends BaseDto {

    public static CountryDto of(Country instance, String translation) {
        return new CountryDto(instance.getIdCode(), instance.getTranslation(translation));
    }

    @Getter
    private String isoCode;
    @Getter
    private String name;

    public CountryDto(String idCode, Country.Translation version) {
        this.isoCode = idCode;
        this.name = version.getName();
    }
}
