/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Country;
import ca.wusc.uniterra.rc.publish.domain.CountryTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class CountryTranslationConverterTest {
    
    private static ObjectMapper mapper = new ObjectMapper();
    
    private CountryTranslationConverter instance;
    
    public CountryTranslationConverterTest() {
    }
    
    @Before
    public void setUp() {
       instance = new CountryTranslationConverter();
    }
    
    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of convertToDatabaseColumn method, of class CountryTranslationConverterTest.
     */
    @Test
    public void testConvertToDatabaseColumn() {
        System.out.println("testConvertToDatabaseColumn");
        List<Country.Translation> versions = CountryTest.getTranslations();
        
        String expResult = "[{\"langIso\":\"fr\",\"name\":\"Russie\"},{\"langIso\":\"en\",\"name\":\"Russia\"}]";
        String result = instance.convertToDatabaseColumn(versions);
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testConvertToEntityAttribute() {
        System.out.println("testConvertToEntityAttribute");
        String input = "[{\"langIso\":\"fr\",\"name\":\"Russie\"},{\"langIso\":\"en\",\"name\":\"Russia\"}]";
        List<Country.Translation> expResult = CountryTest.getTranslations();
        List<Country.Translation> result = instance.convertToEntityAttribute(input);
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        System.out.println(result);
    }
}
