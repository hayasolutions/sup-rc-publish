/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jacques
 * Created: 17-Feb-2017
 * Version: 0.0.1
 * Type: MySQL
 */

/* Structure */

CREATE TABLE `rf_competence` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(10) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_aih6d5sb3a8xan6svwnqy83yi` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_country` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(3) NOT NULL,
  `language` varchar(2) DEFAULT 'en',
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_egbsacwl27cpc9um7js16lvqm` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_language` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(2) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_qxo2w25fjbpagphr1pamxkam5` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_organisation` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(10) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_c7jwl08ukiwd17byip3djtr4u` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_program` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(10) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_nxx8clc1209srg53j6fye5v9t` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_sector` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(10) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_6uen3r88f4a5nnw1imbdxijwe` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rf_stream` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_code` varchar(10) NOT NULL,
  `json_translations` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_ncb0un6r6pi993mlj28bbboo9` (`id_code`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;

CREATE TABLE `rc_jobpost` (
  `pkey` bigint(20) NOT NULL AUTO_INCREMENT,
  `apply_email` varchar(255) NOT NULL,
  `apply_until_date` date NOT NULL,
  `competence` varchar(255) NOT NULL,
  `country` varchar(3) NOT NULL,
  `departure_date` date NOT NULL,
  `duration` int(11) NOT NULL,
  `id_code` varchar(10) NOT NULL,
  `json_translations` longblob DEFAULT NULL,
  `language` varchar(2) NOT NULL,
  `organisation` varchar(255) NOT NULL,
  `program` varchar(255) NOT NULL,
  `publish_date` date DEFAULT NULL,
  `sector` varchar(255) NOT NULL,
  `stream` varchar(255) NOT NULL,
  PRIMARY KEY (`pkey`),
  UNIQUE KEY `UK_1tfqdf1cyoy22rpohm7frncrq` (`id_code`),
  KEY `job_organisation_idx` (`organisation`),
  KEY `job_program_idx` (`program`),
  KEY `job_stream_idx` (`stream`),
  KEY `job_country_idx` (`country`),
  KEY `job_sector_idx` (`sector`),
  KEY `job_competence_idx` (`competence`),
  KEY `job_language_idx` (`language`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;



/*Data*/

