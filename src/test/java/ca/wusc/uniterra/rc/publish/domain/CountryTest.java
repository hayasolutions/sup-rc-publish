/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.domain;

import static ca.wusc.uniterra.rc.publish.domain.CountryTest.getFixture;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class CountryTest {
    private static ObjectMapper mapper = new ObjectMapper();
    
    public static Country getFixture() {
        return Country.builder()
                .idCode("RU")
                .language("en")
                .translations(getTranslations())
                .build();
    }

    public static List<Country.Translation> getTranslations() {
        List<Country.Translation> versions = new ArrayList<>();
        versions.add(Country.Translation.builder()
                .langIso("fr")
                .name("Russie")
                .build());
        versions.add(Country.Translation.builder()
                .langIso("en")
                .name("Russia")
                .build());
        return versions;
    }

    private Country instance;

    public CountryTest() {
    }

    @Before
    public void setUp() {
        instance = getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    public void testToString() {
        System.out.println("testToString");
        System.out.println(instance.toString());
    }
    
    @Test
    public void testWriteJsonObject() throws JsonProcessingException {
        System.out.println("testWriteJsonObject");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Pretty result = " + result);
        System.out.println("Result = " + mapper.writeValueAsString(instance));
    }
    
    
    @Test
    public void testReadJsonObject() throws JsonProcessingException, IOException {
        System.out.println("testReadJsonObject");
        String input = mapper.writeValueAsString(instance);
        Country result = mapper.readValue(input, Country.class);
        assertNotNull(result);
        assertEquals("RU", result.getIdCode());
        assertEquals("Russia", result.getTranslation("en").getName());
        System.out.println("Result = " + result.toString());
    }

    
    
    @Test
    public void testGetTranslation() {
        System.out.println("testToString");
        assertEquals("Russie", instance.getTranslation("fr").getName());
        assertEquals("Russia", instance.getTranslation("en").getName());
    }
   
    @Test
    public void testCanEqualFalse() {
        System.out.println("canEqualFalse");
        Object other = "Hello";
        boolean expResult = false;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }

    @Test
    public void testCanEqualTrue() {
        System.out.println("canEqualTrue");
        Country other = getFixture();
        boolean expResult = true;
        boolean result = instance.canEqual(other);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testEqualsFalse() {
        System.out.println("equals");
        Object o = null;
        boolean expResult = false;
        boolean result = instance.equals(o);
        assertEquals(expResult, result);
    }

    @Test
    public void testHashCode() {
        System.out.println("hashCodeEquals");
        int result = instance.hashCode();
        Country other = getFixture();
        assertEquals(other.hashCode(), result);
        System.out.println("hashcode = " + result);
    }

    @Test
    public void testGetPkey() {
        System.out.println("getPkey");
        Long expResult = null;
        Long result = instance.getPkey();
        assertEquals(expResult, result);
    }
}
