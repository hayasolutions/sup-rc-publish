/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.utils;

import ca.wusc.uniterra.rc.publish.domain.Program;
import ca.wusc.uniterra.rc.publish.domain.ProgramTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jacques
 */
public class ProgramTranslationConverterTest {

    private static ObjectMapper mapper = new ObjectMapper();

    private ProgramTranslationConverter instance;

    public ProgramTranslationConverterTest() {
    }

    @Before
    public void setUp() {
        instance = new ProgramTranslationConverter();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of convertToDatabaseColumn method, of class
 ProgramTranslationConverterTest.
     */
    @Test
    public void testConvertToDatabaseColumn() throws JsonProcessingException {
        System.out.println("testConvertToDatabaseColumn");
        List<Program.Translation> trns = ProgramTest.getTranslations();
        String expResult = mapper.writeValueAsString(trns);
        String result = instance.convertToDatabaseColumn(trns);
        assertEquals(expResult, result);
        System.out.println(result);
    }

    @Test
    public void testConvertToEntityAttribute() throws JsonProcessingException {
        System.out.println("testConvertToEntityAttribute");
        List<Program.Translation> expResult = ProgramTest.getTranslations();
        String input = mapper.writeValueAsString(expResult);
        List<Program.Translation> result = instance.convertToEntityAttribute(input);
        assertEquals(expResult.size(), result.size());
        assertEquals(expResult.get(0), result.get(0));
        System.out.println(result);
    }
}
