/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.service;

import ca.wusc.uniterra.rc.publish.domain.Competence;
import ca.wusc.uniterra.rc.publish.repo.CompetenceRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 *
 * @author jacques
 */
@Service
public class CompetenceSvc {

    @Autowired
    private CompetenceRepo countryRepo;
    
    @Cacheable("competence")
    public Competence getCompetence(String idCode) {
        return countryRepo.findByIdCode(idCode);
    }
    
}
