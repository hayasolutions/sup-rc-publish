/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.repo;

import ca.wusc.uniterra.rc.publish.domain.CompetenceTest;
import ca.wusc.uniterra.rc.publish.domain.Competence;
import org.junit.After;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jacques
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@EntityScan("ca.wusc.uniterra.rc.publish.domain")
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class CompetenceRepoTest {

    @Autowired
    private CompetenceRepo repo;

    @Autowired
    private TestEntityManager em;

    private Competence instance;

    public CompetenceRepoTest() {
    }

    @Before
    public void setUp() {
        instance = CompetenceTest.getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    //@Rollback(false)
    public void testSaveAndCheck() {
        System.out.println("testSaveAndCheck");
        System.out.println("Data = " + instance.toString());
        instance = em.persist(instance);
        assertNotNull(instance.getPkey());
        assertTrue(instance.getPkey() > 0);
        assertNotNull(repo.findOne(instance.getPkey()));
        System.out.println("Result = " + instance.toString());
    }

    @Test
    //@Rollback(false)
    public void testFindByCode() {
        System.out.println("testFindByCode");
        instance = em.persist(instance);
        Competence result = this.repo.findByIdCode("MUSIC");
        assertNotNull(result);
    }
    
    @Test
    public void testFindExistingOne() {
        System.out.println("testFindExistingOne");
        Competence result = repo.findOne(1001L); 
        assertNotNull(result);
        System.out.println(result.toString());
    }
    @Test
    //@Rollback(false)
    public void testFindExistingByCode() {
        System.out.println("testFindExistingByCode");
        //em.persist(instance);
        Competence result = this.repo.findByIdCode("MRKT");
        assertNotNull(result);
        System.out.println(result.toString());
    }
}
