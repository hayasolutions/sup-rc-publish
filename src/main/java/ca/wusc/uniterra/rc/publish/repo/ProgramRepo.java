/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.repo;

import ca.wusc.uniterra.rc.publish.domain.Program;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author jacques
 */
public interface ProgramRepo extends CrudRepository<Program, Long> {

    public Program findByIdCode(String code);

}
