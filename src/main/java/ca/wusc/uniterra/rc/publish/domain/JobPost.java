package ca.wusc.uniterra.rc.publish.domain;

import ca.wusc.uniterra.rc.publish.utils.JobPostTranslationConverter;
import ca.wusc.uniterra.rc.publish.utils.LocalDateDeserializer;
import ca.wusc.uniterra.rc.publish.utils.LocalDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author jacques
 */
@EqualsAndHashCode(of = "idCode")
@Entity
@ToString
@NoArgsConstructor
@Table(name = "rc_jobpost",
        indexes = {
            @Index(name = "job_organisation_idx", columnList = "organisation")
            ,
            @Index(name = "job_program_idx", columnList = "program")
            ,
            @Index(name = "job_stream_idx", columnList = "stream")
            ,
            @Index(name = "job_country_idx", columnList = "country")
            ,
            @Index(name = "job_sector_idx", columnList = "sector")
            ,
            @Index(name = "job_competence_idx", columnList = "competence")
            ,
            @Index(name = "job_language_idx", columnList = "language")
        }
)

public class JobPost implements Serializable {

    private static final long serialVersionUID = 1L;
   
    @EqualsAndHashCode(of = {"langIso","docLink"})
    @AllArgsConstructor
    @Builder
    @ToString
    public static class Translation {

        @Getter
        private final String langIso;
        @Getter
        private final String title;
        @Getter
        private final String docLink;
        @Getter
        private final String pdfLink;
        @Getter
        private final String location;
        @Getter
        private final String excerpt;
    }

    @Getter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long pkey;

    @Getter
    @Column(unique = true, nullable = false, length = 10)
    private String idCode;  //unique job opening ID
    
    @Getter
    @Version @Column(name = "record_version")
    private Integer version;

    @JsonProperty
    @Column(name = "json_translations", columnDefinition = "longblob")
    @Convert(converter = JobPostTranslationConverter.class)
    @Getter(AccessLevel.PACKAGE)
    public List<JobPost.Translation> translations;

    @Getter
    @Column(columnDefinition = "date", nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    private  LocalDate publishDate;
    @Getter
    @Column(columnDefinition = "date", nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    private  LocalDate applyUntilDate;
    @Getter
    @Column(columnDefinition = "date", nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class)  
    private  LocalDate departureDate;
    @Getter
    @Column(nullable = false)
    private  int duration;
    @Getter
    @Column(nullable = false, length = 120)
    private  String applyEmail;
    @Getter
    @Column(name = "organisation", nullable = false, length = 10)
    private  String organisation;
    @Getter
    @Column(name = "program", nullable = false, length = 10)
    private  String program;
    @Getter
    @Column(name = "stream", nullable = false, length = 10)
    private  String stream;
    @Getter
    @Column(name = "country", length = 3, nullable = false)
    private  String country;
    @Getter
    @Column(name = "sector", nullable = false, length = 10)
    private  String sector;
    @Getter
    @Column(name = "competence", nullable = false, length = 10)
    private  String competence;
    @Getter
    @Column(name = "language", length = 2, nullable = false)
    private  String language;

    @Builder
    private JobPost(String idCode, List<JobPost.Translation> translations,
            LocalDate publishDate, LocalDate applyUntilDate, LocalDate departureDate,
            int duration, String applyEmail, String organisation, String country, String program,
            String language, String sector, String competence, String stream) {
        //pass Translation class clazz to parent constructor
        this.idCode = idCode;
        this.publishDate = publishDate;
        this.applyUntilDate = applyUntilDate;
        this.departureDate = departureDate;
        this.duration = duration;
        this.applyEmail = applyEmail;
        this.organisation = organisation;
        this.country = country;
        this.program = program;
        this.language = language;
        this.sector = sector;
        this.competence = competence;
        this.stream = stream;
        this.translations = translations;
    }

    /*Methods*/
    public JobPost.Translation getTranslation(String iso) {
        return (JobPost.Translation) translations.stream()
                .filter(v -> v.getLangIso().equals(iso)).findFirst()
                .orElse(translations.get(0));
    }
    
}
