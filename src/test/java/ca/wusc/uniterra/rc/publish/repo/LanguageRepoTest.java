/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.repo;

import ca.wusc.uniterra.rc.publish.domain.Language;
import ca.wusc.uniterra.rc.publish.domain.LanguageTest;
import org.junit.After;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author jacques
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@EntityScan("ca.wusc.uniterra.rc.publish.domain")
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class LanguageRepoTest {

    private Language instance;

    @Autowired
    private LanguageRepo repo;

    @Autowired
    private TestEntityManager em;

    public LanguageRepoTest() {
    }

    @Before
    public void setUp() {
        instance = LanguageTest.getFixture();
    }

    @After
    public void tearDown() {
        instance = null;
    }

    @Test
    //@Rollback(false)
    public void testSaveAndCheck() {
        System.out.println("testSaveAndCheck");
        System.out.println("Data = " + instance.toString());
        instance = em.persist(instance);
        assertNotNull(instance.getPkey());
        assertTrue(instance.getPkey() > 0);
        Language result = repo.findOne(instance.getPkey());
        assertNotNull(result);
        System.out.println("Result = " + instance.toString());
    }

    @Test
    //@Rollback(false)
    public void testFindByCode() {
        System.out.println("findByCode");
        em.persist(instance);
        Language result = this.repo.findByIdCode("fr");
        assertNotNull(result);
        System.out.println("Result = " + result.toString());
    }

    @Test
    public void testFindExistingOne() {
        System.out.println("testFindExistingOne");
        Language result = repo.findOne(1000L); 
        assertNotNull(result);
        System.out.println("Result = " + result.toString());
    }
    @Test
    //@Rollback(false)
    public void testFindExistingByCode() {
        System.out.println("testFindExistingByCode");
        Language result = this.repo.findByIdCode("en");
        assertNotNull(result);
        System.out.println("Result = " + result.toString());
    }
}
