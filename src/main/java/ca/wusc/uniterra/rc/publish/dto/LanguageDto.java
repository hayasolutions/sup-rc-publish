/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Language;
import lombok.Getter;

/**
 *
 * @author jacques
 */
public class LanguageDto extends BaseDto {
    
    public static LanguageDto of(Language instance, String isoLang) {
       return new LanguageDto(instance.getIdCode(), instance.getTranslation(isoLang)); 
    }
    
    @Getter
    private String isoCode;
    @Getter
    private String name;

    private LanguageDto(String idCode, Language.Translation version) {
        this.isoCode = idCode;
        this.name = version.getName();
    }

}
