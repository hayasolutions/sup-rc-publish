/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.dto;

import ca.wusc.uniterra.rc.publish.domain.Organisation;
import ca.wusc.uniterra.rc.publish.domain.OrganisationTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jacques
 */
public class OrganisationDtoTest {
    
    public static OrganisationDto getFixture(String isoLang) {
        Organisation country = OrganisationTest.getFixture();
        return OrganisationDto.of(country, isoLang);
    }
    
    private ObjectMapper mapper = new ObjectMapper();
    private OrganisationDto instance;
    
    public OrganisationDtoTest() {
    }
    
    @Before
    public void setUp() {
        this.instance = getFixture("en");
    }
    
    @After
    public void tearDown() {
        this.instance = null;
    }

    @Test
    public void testSerialization() throws JsonProcessingException {
        System.out.println("testSerialization");
        String json = mapper.writeValueAsString(instance);
        System.out.println("Json serialization = " + json);
    }
    @Test
    public void testPrettySerializing() throws JsonProcessingException {
        System.out.println("testPrettySerializing");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Serialized OrganisationDto = " + result);
    }
    
    
}
