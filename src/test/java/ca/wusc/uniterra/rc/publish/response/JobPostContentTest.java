/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.wusc.uniterra.rc.publish.response;

import ca.wusc.uniterra.rc.publish.dto.CompetenceDtoTest;
import ca.wusc.uniterra.rc.publish.dto.CountryDtoTest;
import ca.wusc.uniterra.rc.publish.dto.JobPostDtoTest;
import ca.wusc.uniterra.rc.publish.dto.LanguageDtoTest;
import ca.wusc.uniterra.rc.publish.dto.OrganisationDtoTest;
import ca.wusc.uniterra.rc.publish.dto.ProgramDtoTest;
import ca.wusc.uniterra.rc.publish.dto.SectorDtoTest;
import ca.wusc.uniterra.rc.publish.dto.StreamDtoTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jacques
 */
public class JobPostContentTest {
    
    public static JobPostContent getFixture(String isoLang) {
        JobPostContent content = JobPostContent.builder()
                .jobpost(JobPostDtoTest.getFixture(isoLang))
                .country(CountryDtoTest.getFixture(isoLang))
                .language(LanguageDtoTest.getFixture(isoLang))
                .organisation(OrganisationDtoTest.getFixture(isoLang))
                .program(ProgramDtoTest.getFixture(isoLang))
                .sector(SectorDtoTest.getFixture(isoLang))
                .stream(StreamDtoTest.getFixture(isoLang))
                .competence(CompetenceDtoTest.getFixture(isoLang))
                .build();
        return content;
    }
    
    private ObjectMapper mapper = new ObjectMapper();
    private JobPostContent instance;
    
    public JobPostContentTest() {
    }
    
    @Before
    public void setUp() {
      this.instance = getFixture("en");
    }
    
    @After
    public void tearDown() {
        this.instance = null;
    }


    @Test
    public void testSerialization() throws JsonProcessingException {
        System.out.println("testSerialization");
        String json = mapper.writeValueAsString(instance);
        System.out.println("Json serialization = " + json);
    }
    @Test
    public void testPrettySerializing() throws JsonProcessingException {
        System.out.println("testPrettySerializing");
        String result = mapper.writer().withDefaultPrettyPrinter().writeValueAsString(instance);
        System.out.println("Serialized JobPostDto = " + result);
    }
    
}
